package archaiusDemoProject.archaiusDemo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;

import com.netflix.config.AbstractPollingScheduler;
import com.netflix.config.ConcurrentCompositeConfiguration;
import com.netflix.config.ConcurrentMapConfiguration;
import com.netflix.config.ConfigurationManager;
import com.netflix.config.DynamicConfiguration;
import com.netflix.config.DynamicPropertyFactory;
import com.netflix.config.DynamicStringProperty;
import com.netflix.config.FixedDelayPollingScheduler;
import com.netflix.config.PolledConfigurationSource;
import com.netflix.config.jmx.ConfigJMXManager;
import com.netflix.config.sources.URLConfigurationSource;


/**
 * Reads properties from different sources, creates the hierarchy to override the properties
 * Exposes only the properties over JMX where the source is URL
 * @author msha85
 *
 */
public class UrlSourcePooler {
	
	static {
		
		//property to read the urls
        System.setProperty("archaius.configurationSource.additionalUrls", "https://bitbucket.org/msha85/archaiustest/raw/1715bedf87572a79e45a8a7f0681c0fe8893246a/src/main/java/ApplicationConfig2.properties");
        System.setProperty("archaiusDemoProject.archaiusDemo.value", "System Value");
        //System.setProperty(DynamicPropertyFactory.ENABLE_JMX, "true"); 
	}
	
	 static DynamicStringProperty sensitiveBeanData =  DynamicPropertyFactory 
		      .getInstance() 
		      .getStringProperty( 
		        "hello.world.message2", 
		        "magic");
	 
	
		 
	 public static void main(String[] args) {
		 
		 //call the thread
		 new SampleThread();
		 
		 UrlSourcePooler urlPooler = new UrlSourcePooler();
				 
		 //// configuration from file
		 String fileName = "customConfig.properties";
		 ConcurrentMapConfiguration configFromPropertiesFile  = urlPooler.createfileReaderSource(fileName);
	  	  
		 // configuration from system properties
		 ConcurrentMapConfiguration configFromSystemProperties = urlPooler.createSystemPropReaderSource();
	      
		 //URL Property reader with poll feature added
		 DynamicConfiguration dynamicConfiguration = urlPooler.createURLReaderSource();
	    
		 urlPooler.configureAsMBean(dynamicConfiguration);
		 
		 List<AbstractConfiguration> configurations = new ArrayList<AbstractConfiguration>();
		  // create a hierarchy of configuration that makes
		  // 1) dynamic configuration source override system properties and,
		  // 2) system properties override properties file
		 configurations.add(dynamicConfiguration);
		 configurations.add(configFromSystemProperties);
		 configurations.add(configFromPropertiesFile);
		 
		 urlPooler.createHierarchyAndInstall(configurations);
	  
	  }
	 
	 /**
	  * Generic method to create a file reader
	  * @param fileName
	  * @return
	  */
	 ConcurrentMapConfiguration createfileReaderSource(String fileName){
		 try{
			  return  new ConcurrentMapConfiguration(new PropertiesConfiguration(fileName));  
				  
			  } catch (ConfigurationException e) {
				
				System.out.println(e.getMessage());
				return null;
			}
		 
	 }
	 
	 /**
	  * Generic method to create system property reader
	  * @return
	  */
	 ConcurrentMapConfiguration createSystemPropReaderSource(){
		
			  return  new ConcurrentMapConfiguration(new SystemConfiguration()); 
	 }
	 
	 
	 /**
	  * Generic method to create url property reader
	  * @return
	  */
	 DynamicConfiguration createURLReaderSource(){
		
		  // configuration from a dynamic source (URL)
		  PolledConfigurationSource source = new URLConfigurationSource();
		  //Initial delay, delay, ignore deletes
		  AbstractPollingScheduler scheduler = new FixedDelayPollingScheduler(100, 1000, true);

		  return  new DynamicConfiguration(source, scheduler);
	 }
	 
	 /**
	  * Configure any configuration as MBean to expose via JMX
	  * @param config
	  */
	 void configureAsMBean(DynamicConfiguration config){
		 ConfigJMXManager.registerConfigMbean(config); 
	 }
	 
	 
	 /**
	  * Create the hierarchy from configuration list and install
	  * @param configutations
	  */
	 void createHierarchyAndInstall(List<AbstractConfiguration> configutations){
		 
		  ConcurrentCompositeConfiguration finalConfig = new ConcurrentCompositeConfiguration();
		  if(configutations != null && !configutations.isEmpty()){
			  for(AbstractConfiguration config : configutations){
				  finalConfig.addConfiguration(config);
			  }
			  
		  }
		  ConfigurationManager.install(finalConfig); 
		 
	 }
	
}
