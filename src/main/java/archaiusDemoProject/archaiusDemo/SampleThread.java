package archaiusDemoProject.archaiusDemo;

import com.netflix.config.jmx.ConfigMBean; 
 
/**
 * A Sample Application built just to keep the app up for Jconsole inspect
 * @author msha85
 *
 */
public class SampleThread extends Thread { 
 
	 static ConfigMBean configMBean = null; 
	 
	 public SampleThread() { 
		 setDaemon(false);  
		 start(); 
	 } 
	 
	 /**
	  * infinite loop
	  */
	 public void run() { 
		 while (true) { 
			 try { 
				 System.out.println("Property sensitiveBeanData = " + UrlSourcePooler.sensitiveBeanData.get());
				 sleep(3000); 
			 } catch (InterruptedException e) { 
				 throw new RuntimeException(e); 
			 } 
	    
		 } 
	 } 
	  
} 
 

