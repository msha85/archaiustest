package archaiusDemoProject.archaiusDemo;

import java.io.IOException;

import com.netflix.config.ConfigurationManager;
import com.netflix.config.DynamicPropertyFactory;
import com.netflix.config.DynamicStringProperty;

public class ApplicationConfig {
	
	static {
        System.setProperty("archaius.configurationSource.defaultFileName", "ApplicationConfig.properties");
    }

	public static void main(String[] args) {
	    
	    try {
            ConfigurationManager.loadCascadedPropertiesFromResources("ApplicationConfig");
        } catch (IOException e) {
            e.printStackTrace();
        }
	    
	    ApplicationConfig appConfig = new ApplicationConfig();
    	String property = appConfig.getStringProperty("hello.world.message", "default message");
		System.out.println(property);
		
		String property2 = appConfig.getStringProperty("hello.world.message2", "default message");
		System.out.println(property2);
		
		String property3 = appConfig.getStringProperty("hello.world.message3", "default message");
		System.out.println(property3);

	}
	
	public String getStringProperty(String key, String defaultValue) {
        final DynamicStringProperty property = DynamicPropertyFactory.getInstance().getStringProperty(key,
            defaultValue);
        return property.get();
    }

}
