package archaiusDemoProject.archaiusDemo;

import java.io.IOException;

import com.netflix.config.ConcurrentCompositeConfiguration; 
import com.netflix.config.ConcurrentMapConfiguration; 
import com.netflix.config.ConfigurationManager;
import com.netflix.config.DynamicPropertyFactory; 
import com.netflix.config.DynamicStringProperty;

/**
 * Sample application that reads from file, system and application properties and 
 * exposes then as MBeans 
 * @author msha85
 *
 */
public class SampleJMXApplication {

	
	static { 
		
		//Default properties file name
        System.setProperty("archaius.configurationSource.defaultFileName", "ApplicationConfig.properties"); 
        
        //Exposes all the properties as MBean
        System.setProperty(DynamicPropertyFactory.ENABLE_JMX, "true"); 
        
        //System property
        System.setProperty("archaiusDemoProject.archaiusDemo.sensitiveBeanData", "system property data"); 
    } 
	
	/** This field utilizes the feature of Callbacks. 
     * When a property value is changed at runtime, it will receive a callback 
     * which can be used to trigger other operations 
     */	
    static DynamicStringProperty sensitiveBeanData = DynamicPropertyFactory 
      .getInstance() 
      .getStringProperty( 
        "archaiusDemoProject.archaiusDemo.sensitiveBeanData", 
        "magic", new Runnable() { 
         public void run() { 
          // let my auditing system know about this change 
          System.out 
            .println("SampleBean.sensitiveData changed to:" 
              + sensitiveBeanData.get()); 
         } 
        }); 
    
    
    public static void main(String[] args) { 
    	
    	 //call the thread
        new SampleThread(); 
        
        ConcurrentCompositeConfiguration myConfiguration =  
            (ConcurrentCompositeConfiguration) DynamicPropertyFactory.getInstance().getBackingConfigurationSource();
        
        ConcurrentMapConfiguration subConfig = new ConcurrentMapConfiguration(); 
        subConfig.setProperty("archaiusDemoProject.archaiusDemo.name", "Test1"); 
        myConfiguration.setProperty("archaiusDemoProject.archaiusDemo.prop1", "value1"); 
 
        myConfiguration.addConfiguration(subConfig); 
        
        try {
            ConfigurationManager.loadCascadedPropertiesFromResources("ApplicationConfig");
        } catch (IOException e) {
            e.printStackTrace();
        }   
       
        System.out.println("Started SampleJMXApplication. Launch JConsole to inspect and update properties."); 
        System.out.println("To see how callback work, update archaiusDemoProject.archaiusDemo from BaseConfigBean in JConsole"); 
         
    } 
    
  

}
